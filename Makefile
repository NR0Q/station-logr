.DEFAULT_GOAL := help

DESTDIR ?= ./build
BIN ?= $(DESTDIR)/usr/bin
LIB ?= $(DESTDIR)/usr/share/station-logr
APP ?= $(DESTDIR)/usr/share/applications
MAN ?= $(DESTDIR)/usr/share/man/man1
DOC ?= $(DESTDIR)/usr/share/doc/station-logr
NO_DIST_INSTALL ?=

.PHONY: help
help:
	@echo "Specify a target to build:"
	@echo "  -> make install"
	@echo "    install station-logr in DESTDIR (default: ./build)"
	@echo "  -> make clean"
	@echo "    remove built files from DESTDIR"

.PHONY: clean
clean:
	@echo "Cleaning $(DESTDIR)..."
	rm -rf $(DESTDIR)/*

.PHONY: build
build:
	@echo "Station Logr doesn't need building :D"

.PHONY: install
install:
	@echo "Installing Station Logr in $(DESTDIR)..."
	install -Dcm 755 station-logr.sh $(BIN)/station-logr
	install -Dcm 644 station-logr.desktop $(APP)/station-logr.desktop
	install -Dcm 644 station-logr.1 $(MAN)/station-logr.1
	install -Dcm 644 LICENSE $(DOC)/LICENSE
	mkdir -p $(LIB)
	cp -r package.nw/* $(LIB)
ifndef NO_DIST_INSTALL
	cp -r dist/*-linux-x64/* $(LIB)
endif
