// Station Logr Copyright (C) 2021 Matthew Chambers, CBT
// All rights rreserved.
// See LICENSE for more information.
function startupVersionInit()
{
  if (!sl_devMode)
  {
    document.body.addEventListener("contextmenu", function (ev)
    {
      ev.preventDefault();
    });
  }
}

function postInit()
{
  sl_finishedLoading = true;
}

document.addEventListener("drop", function (event)
{
  event.preventDefault();
  if (sl_finishedLoading == true) dropHandler(event);
});

function importSettings()
{
  checkForSettings();

  var filename = sl_appData + sl_dirSeperator + "sl_settings.json";
  if (fs.existsSync(filename))
  {
    var data = fs.readFileSync(filename);
    data = JSON.parse(data);
    if (
      typeof data.appSettings != "undefined" &&
      data.currentVersion == localStorage.currentVersion
    )
    {
      localStorage.clear();
      for (var key in data)
      {
        localStorage[key] = data[key];
      }
      unlinkSync(filename);
      chrome.runtime.reload();
    }
    else
    {
      if (typeof data.appSettings == "undefined")
      {
        importSettingsFile.innerHTML =
          "<font style='color:red'>Settings File Corrupt!</font>";
      }
      else if (data.currentVersion != localStorage.currentVersion)
      {
        importSettingsFile.innerHTML =
          "<font style='color:red'>Settings Version Mismatch!</font>";
      }
    }
  }
}

function loadStationsJson()
{
  var file = "./data/stations.json";
  if (fs.existsSync(file))
  {
    var fileBuf = fs.readFileSync(file, "UTF-8");
    sl_stationJson = JSON.parse(fileBuf);
  }
}

function loadLatexExport()
{
  var file = "./data/export.tex";
  if (fs.existsSync(file))
  {
    var fileBuf = fs.readFileSync(file, "UTF-8");
    sl_latexExport = fileBuf;
  }
}

var sl_startupTable = [
  [startupVersionInit, "Completed Version Check"],
  [loadStationsJson, "Loaded Station Data"],
  [loadLatexExport, "Loaded Export Template"],
  [postInit, "Finalizing System"]
];

function startupEngine()
{
  if (sl_startupTable.length > 0)
  {
    var funcInfo = sl_startupTable.shift();
    funcInfo[0]();
    startupStatusDiv.innerHTML = funcInfo[1];
    setTimeout(startupEngine, 1000);
  }
  else
  {
    startupStatusDiv.innerHTML = "Completed";
    setTimeout(endStartup, 2000);
  }
}

function directoryInput(what)
{
  sl_appSettings.savedAppData = what.files[0].path;
  init();
}

function endStartup()
{
  startupDiv.style.display = "none";
  main.style.display = "block";
  renderStationArray();
}

function is_dir(path)
{
  try
  {
    var stat = fs.lstatSync(path);
    return stat.isDirectory();
  }
  catch (e)
  {
    // lstatSync throws an error if path doesn't exist
    console.log(e);
    return false;
  }
}

function mediaCheck()
{
  var homeDir = (sl_platform == "windows") ? process.env.USERPROFILE : process.env.HOME;
  sl_appData = path.join(homeDir, "OneDrive\\Dokumente");
  if (!is_dir(sl_appData))
  {
    sl_appData = path.join(homeDir, "OneDrive\\Documents");
    if (!is_dir(sl_appData))
    {
      sl_appData = path.join(homeDir, "Dokumente")
      if (!is_dir(sl_appData))
      {
        sl_appData = path.join(homeDir, "Documents")
        if (!is_dir(sl_appData))
        {
          if (sl_appSettings.savedAppData != null)
          {
            sl_appData = sl_appSettings.savedAppData;
            if (!is_dir(sl_appData)) return false;
          }
          else
          {
            return false;
          }
        }
      }
    }
  }
  sl_appData = path.join(sl_appData, "StationLogr");
  sl_stationLogos = path.join(sl_appData, "logos");
  sl_jsonDir = path.join(sl_appData, "data");
  sl_NWappData = path.join(nw.App.dataPath, "sl-internal");
  try
  {
    var tryDirectory = "";
    var userdirs = [
      sl_appData,
      sl_stationLogos,
      sl_jsonDir,
      sl_NWappData
    ];
    for (var dir of userdirs)
    {
      if (!fs.existsSync(dir))
      {
        tryDirectory = dir;
        fs.mkdirSync(dir);
        if (dir === sl_stationLogos)
        {
          var fileList = [].concat(fs.readdirSync("./img/logos/"));
          fileList.forEach(function (logofile)
          {
            fs.copyFileSync("./img/logos/" + logofile, sl_stationLogos + sl_dirSeperator + logofile);
          })
        }
      }
    }
  }
  catch (e)
  {
    alert(
      "Unable to create or access " + tryDirectory + " folder.\r\nPermission violation, Station Logr cannot continue"
    );
    nw.App.quit();
  }
  sl_appData += sl_dirSeperator;
  sl_jsonDir += sl_dirSeperator;
  sl_stationLogos += sl_dirSeperator;
  sl_NWappData += sl_dirSeperator;

  return true;
}
