// Station Logr Copyright (C) 2021 Matthew Chambers, CBT
// All rights rreserved.
// See LICENSE for more information.
function priorLogChange(stationCall)
{
  var newLoadedLog = oldLogDates.options[oldLogDates.selectedIndex].value;
  stationLogForm(stationCall, newLoadedLog)
}

function stationLogForm(stationCall, loadedLog)
{
  var worker = ""
  var stationData = sl_stationJson[stationCall];
  stationCallsign.innerHTML = "<h1>" + stationCall + "</h1>";
  if (stationData.logo !== "undefined" && fs.existsSync(sl_stationLogos + stationData.logo))
  {
    stationLogo.innerHTML = "<img src='file:" + sl_stationLogos + stationData.logo + "'>";
  }
  else
  {
    stationLogo.innerHTML = "";
  }
  worker = "";
  Object.keys(stationData.metadata).forEach(function (key)
  {
    worker += metadataLabels(key) + stationData.metadata[key] + "<br />";
  });
  stationMetadata.innerHTML = worker;
  worker = "";
  if (ifLogExists(stationCall))
  {
    var oldData = readLogFile(stationCall);
    worker += "<label for='oldLogDates'>Prior Log Data</label><select id='oldLogDates' onchange='priorLogChange(\"" + stationCall + "\")'>";
    worker += "<option id='logNew' value='logNew'>Log New Data</option>";
    Object.keys(oldData[stationCall]).forEach(function (oldDate)
    {
      worker += "<option id='oldLog-" + oldDate + "' value='" + oldDate + "'";
      if (oldDate === loadedLog)
      {
        worker += " selected";
      }
      worker += ">" + oldDate + "</option>";
    });
    worker += "</select>";
  }
  else
  {
    worker += "No prior log data found.";
  }
  oldLogs.innerHTML = worker;
  worker = "<hr>";
  var logTime = "";
  if (loadedLog === "logNew")
  {
    logTime = dateToISO8601(currentTimeStampString(), Intl.DateTimeFormat().resolvedOptions().timeZone);
    logTime = logTime.slice(0, 23);
  }
  else
  {
    logTime = loadedLog;
  }
  worker += "<div id='logDateTime'><input class='logInput' type='datetime-local' id='logDateTime' name='logDateTime' value='" + logTime + "'/></div><table><tr>";
  Object.keys(stationData.log).forEach(function (key, i)
  {
    var logItem = stationData.log[key];
    worker += "<td><div id='div_" + key + "'class='logDiv'><label for='" + key + "'>" + logItem.label;
    if (logItem.type == "number")
    {
      worker += " (" + logItem.units + ")";
    }
    worker += "</label>";
    if (logItem.type !== "dropdown" && logItem.type !== "status")
    {
      worker += "<input class='logInput' type='" + logItem.type + "' id='" + key + "' name='" + key + "' ";
      Object.keys(logItem).forEach(function (params)
      {
        if (params !== "type" && params !== "label" && params !== "required" && params !== "units")
        {
          worker += params + "='" + logItem[params] + "' ";
        }
        else if (params == "required")
        {
          worker += "required ";
        }
      });
      worker += ">";
    }
    else if (logItem.type == "dropdown")
    {
      worker += "<select class='logInput' id='" + key + "' name='" + key + "'/>";
      Object.keys(logItem.options).forEach(function (option)
      {
        worker += "<option id='" + logItem.options[option] + "' name='" + logItem.options[option] + "'";
        if (loadedLog !== "logNew" && logItem.options[option] == oldData[stationCall][loadedLog][key])
        {
          worker += " selected";
        }
        worker += "/>" + logItem.options[option] + "</option>";
      })
      worker += "</select>"
    }
    else if (logItem.type == "status")
    {
      worker += "<input type='checkbox' id='" + key + ", name='" + key + "'/>";
    }
    worker += "</td></div>";
    if ((i + 1) % 2 === 0)
    {
      worker += "</tr><tr>";
    }
  });
  worker += "</tr></table></div>"
  stationLogData.innerHTML = worker;
  var logElements = document.getElementsByClassName("logInput");
  Object.keys(logElements).forEach(function (element)
  {
    if (loadedLog !== "logNew")
    {
      logElements[element].setAttribute("readonly", true);
      if (logElements[element].type !== "undefined" && logElements[element].type !== "datetime-local")
      {
        logElements[element].value = oldData[stationCall][loadedLog][logElements[element].id];
      }
      if (logElements[element].nodeName === "SELECT")
      {
        logElements[element].style = "pointer-events: none";
      }
      saveStationLog.style.display = "none";
    }
    else
    {
      if (logElements[element].type !== "undefined" && logElements[element].type !== "datetime-local")
      {
        logElements[element].value = "";
      }
      saveStationLog.style.display = "inline-block";
    }
  });
  stationList.style.display = "none";
  doExportLogs.style.display = "inline-block";
  exportStationLogs.style.display = "none";
  returnToList.style.display = "inline-block";
  stationForm.style.display = "block";
}

function saveLog()
{
  var logElements = document.getElementsByClassName("logInput");
  var stationCall = document.getElementById("stationCallsign").innerText;

  var logObject = createLogObject(logElements);

  if (ifLogExists(stationCall))
  {
    appendLogFile(stationCall, logObject);
  }
  else
  {
    saveNewFile(stationCall, logObject);
  }
  alert("The log for station " + stationCall + " was saved!");
  stationForm.style.display = "none";
  stationList.style.display = "block";
  returnToList.style.display = "none";
  saveStationLog.style.display = "none";
}

function metadataLabels(key)
{
  var label = "";
  switch (key)
  {
    case "facilityID":
      label = "Facility ID: ";
      break;
    case "service":
      label = "Service: ";
      break;
    case "class":
      label = "Class: ";
      break;
    case "city":
      label = "City of License: ";
      break;
    case "freq":
      label = "Frequency: ";
      break;
    case "power":
      label = "Power: ";
      break;
    case "time":
      label = "Broadcast Schedule: ";
      break;
    case "location":
      label = "Location (NAD83): "
      break;
    case "channel":
      label = "Channel: ";
      break;
    case "haat":
      label = "Antenna Height (HAAT): ";
      break;
    case "parent":
      label = "Translator Parent Station: ";
      break;
    default:
      label = key + ": ";
  }
  return label;
}
