// Station Logr Copyright (C) 2021 Matthew Chambers, CBT
// All rights rreserved.
// See LICENSE for more information.
const pjson = require("./package.json");
var slVersion = parseInt(pjson.version.replace(/\./g, ""));
var slBeta = pjson.betaVersion;

var sl_appSettings = {};
var sl_stationJson = {};
var sl_latexExport = {};

var sl_startVersion = 0;
if (typeof localStorage.currentVersion != "undefined") { slStartVersion = localStorage.currentVersion; }

if (typeof localStorage.currentVersion == "undefined" || localStorage.currentVersion != String(slVersion))
{
  localStorage.currentVersion = String(slVersion);
  var gui = require("nw.gui");
  gui.App.clearCache();
}

var vers = String(slVersion);
var slShortVersion = "v" + pjson.version + " " + slBeta;
var slVersionString = "Station Logr " + slShortVersion;

var sl_windowName = "sl-main";
const os = require("os");
const fs = require("fs");
const latex = require("node-latex");
const snmp = require("net-snmp");
const process = require("process");
const path = require("path");
const sl_dirSeperator = path.sep;

var sl_platform = os.platform();
if (sl_platform.indexOf("win") == 0 || sl_platform.indexOf("Win") == 0)
{
  sl_platform = "windows";
}
if (sl_platform.indexOf("inux") > -1) sl_platform = "linux";
if (sl_platform.indexOf("darwin") > -1) sl_platform = "mac";

var gui = require("nw.gui");
const { start } = require("repl");
var win = gui.Window.get();

var sl_devMode = process.versions["nw-flavor"] == "sdk";

function loadAllSettings()
{
  for (var x in localStorage)
  {
    if (!validSettings.includes(x) && typeof localStorage[x] == "string")
    {
      delete localStorage[x];
    }
  }
  g_appSettings = loadDefaultsAndMerge("appSettings", def_appSettings);
}

loadAllSettings();

function loadDefaultsAndMerge(key, def)
{
  var settings = {};
  if (typeof localStorage[key] != "undefined")
  {
    settings = JSON.parse(localStorage[key]);
  }
  var merged = deepmerge(def, settings);
  for (var x in merged)
  {
    if (!(x in def))
    {
      delete merged[x];
    }
  }
  localStorage[key] = JSON.stringify(merged);
  return merged;
}

function loadArrayIfExists(key)
{
  var data = [];
  if (typeof localStorage[key] != "undefined")
  {
    data = JSON.parse(localStorage[key]);
  }
  return data;
}

function loadObjectIfExists(key)
{
  var data = {};
  if (typeof localStorage[key] != "undefined")
  {
    data = JSON.parse(localStorage[key]);
  }
  return data;
}

function saveAndCloseApp()
{
  sl_closing = true;
  nw.App.quit();
}

win.on("close", function ()
{
  saveAndCloseApp();
});
win.show();
win.setMinimumSize(200, 600);

var sl_closing = false;

function init()
{
  startupVersionDiv.innerHTML = slVersionString;
  // aboutVersionDiv.innerHTML = slVersionString;
  sl_currentDay = parseInt(timeNowSec() / 86400);
  if (mediaCheck() == false)
  {
    startupDiv.style.display = "none";
    documentsDiv.style.display = "block";
    searchedDocFolder.innerHTML = sl_appData;
  }
  else
  {
    documentsDiv.style.display = "none";
    startupDiv.style.display = "block";
    startupStatusDiv.innerHTML = "Starting...";
    setTimeout(startupEngine, 10);
  }
}

function saveAppSettings()
{
  localStorage.appSettings = JSON.stringify(sl_appSettings);
}
