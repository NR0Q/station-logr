function isMergeableObject(val)
{
  var nonNullObject = val && typeof val === "object"

  return nonNullObject &&
    Object.prototype.toString.call(val) !== "[object RegExp]" &&
    Object.prototype.toString.call(val) !== "[object Date]"
}

function emptyTarget(val)
{
  return Array.isArray(val) ? [] : {}
}

function cloneIfNecessary(value, optionsArgument)
{
  var clone = optionsArgument && optionsArgument.clone === true
  return (clone && isMergeableObject(value)) ? deepmerge(emptyTarget(value), value, optionsArgument) : value
}

function defaultArrayMerge(target, source, optionsArgument)
{
  var destination = target.slice()
  source.forEach(function (e, i)
  {
    if (typeof destination[i] === "undefined")
    {
      destination[i] = cloneIfNecessary(e, optionsArgument)
    }
    else if (isMergeableObject(e))
    {
      destination[i] = deepmerge(target[i], e, optionsArgument)
    }
    else if (target.indexOf(e) === -1)
    {
      destination.push(cloneIfNecessary(e, optionsArgument))
    }
  })
  return destination
}

function mergeObject(target, source, optionsArgument)
{
  var destination = {}
  if (isMergeableObject(target))
  {
    Object.keys(target).forEach(function (key)
    {
      destination[key] = cloneIfNecessary(target[key], optionsArgument)
    })
  }
  Object.keys(source).forEach(function (key)
  {
    if (!isMergeableObject(source[key]) || !target[key])
    {
      destination[key] = cloneIfNecessary(source[key], optionsArgument)
    }
    else
    {
      destination[key] = deepmerge(target[key], source[key], optionsArgument)
    }
  })
  return destination
}

function deepmerge(target, source, optionsArgument)
{
  var array = Array.isArray(source);
  var options = optionsArgument || { arrayMerge: defaultArrayMerge }
  var arrayMerge = options.arrayMerge || defaultArrayMerge

  if (array)
  {
    return Array.isArray(target) ? arrayMerge(target, source, optionsArgument) : cloneIfNecessary(source, optionsArgument)
  }
  else
  {
    return mergeObject(target, source, optionsArgument)
  }
}

deepmerge.all = function deepmergeAll(array, optionsArgument)
{
  if (!Array.isArray(array) || array.length < 2)
  {
    throw new Error("first argument should be an array with at least two elements")
  }

  // we are sure there are at least 2 values, so it is safe to have no initial value
  return array.reduce(function (prev, next)
  {
    return deepmerge(prev, next, optionsArgument)
  })
}

// Source: http://stackoverflow.com/questions/497790
var dates = {
  convert: function(d)
  {
    // Converts the date in d to a date-object. The input can be:
    //   a date object: returned without modification
    //  an array      : Interpreted as [year,month,day]. NOTE: month is 0-11.
    //   a number     : Interpreted as number of milliseconds
    //                  since 1 Jan 1970 (a timestamp)
    //   a string     : Any format supported by the javascript engine, like
    //                  "YYYY/MM/DD", "MM/DD/YYYY", "Jan 31 2009" etc.
    //  an object     : Interpreted as an object with year, month and date
    //                  attributes.  **NOTE** month is 0-11.
    return (
      d.constructor === Date
        ? d
        : d.constructor === Array
          ? new Date(d[0], d[1], d[2])
          : d.constructor === Number
            ? new Date(d)
            : d.constructor === String
              ? new Date(d)
              : typeof d === "object"
                ? new Date(d.year, d.month, d.date)
                : NaN
    );
  },
  compare: function(a, b)
  {
    // Compare two dates (could be of any type supported by the convert
    // function above) and returns:
    //  -1 : if a < b
    //   0 : if a = b
    //   1 : if a > b
    // NaN : if a or b is an illegal date
    // NOTE: The code inside isFinite does an assignment (=).
    return (
      isFinite(a = this.convert(a).valueOf()) &&
          isFinite(b = this.convert(b).valueOf())
        ? (a > b) - (a < b)
        : NaN
    );
  },
  inRange: function(d, start, end)
  {
    // Checks if date in d is between dates in start and end.
    // Returns a boolean or NaN:
    //    true  : if d is between start and end (inclusive)
    //    false : if d is before start or after end
    //    NaN   : if one or more of the dates is illegal.
    // NOTE: The code inside isFinite does an assignment (=).
    return (
      isFinite(d = this.convert(d).valueOf()) &&
          isFinite(start = this.convert(start).valueOf()) &&
          isFinite(end = this.convert(end).valueOf())
        ? start <= d && d <= end
        : NaN
    );
  }
}
