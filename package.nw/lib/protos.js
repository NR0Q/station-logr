Number.prototype.toDHMS = function ()
{
  var seconds = this;
  var days = Math.floor(seconds / (3600 * 24));
  seconds -= days * 3600 * 24;
  var hrs = Math.floor(seconds / 3600);
  seconds -= hrs * 3600;
  var mnts = Math.floor(seconds / 60);
  seconds -= mnts * 60;

  days = days ? days + "d " : "";
  hrs = hrs ? hrs + "h " : "";
  mnts = mnts ? mnts + "m " : "";
  var first = days + hrs + mnts;
  if (first == "") val = seconds + "s";
  else val = first + (seconds > 0 ? seconds + "s" : "");
  return val;
};

Number.prototype.toDHM = function ()
{
  var seconds = this;
  var days = Math.floor(seconds / (3600 * 24));
  seconds -= days * 3600 * 24;
  var hrs = Math.floor(seconds / 3600);
  seconds -= hrs * 3600;
  var mnts = Math.floor(seconds / 60);
  seconds -= mnts * 60;

  days = days ? days + "d " : "";
  hrs = hrs ? hrs + "h " : "";
  mnts = mnts ? mnts + "m " : "";
  val = days + hrs + mnts;
  return val;
};

Number.prototype.toYM = function ()
{
  var months = this;
  var years = parseInt(Math.floor(months / 12));
  months -= years * 12;
  months = parseInt(months);
  years = years ? years + "y " : "";
  months = months ? months + "m" : "";
  var total = years + months;
  return total == "" ? "any" : total;
};

Number.prototype.toHMS = function ()
{
  var seconds = this;
  var days = Math.floor(seconds / (3600 * 24));
  seconds -= days * 3600 * 24;
  var hrs = Math.floor(seconds / 3600);
  seconds -= hrs * 3600;
  var mnts = Math.floor(seconds / 60);
  seconds -= mnts * 60;

  hrs = hrs < 10 ? "0" + hrs : hrs;
  mnts = mnts < 10 ? "0" + mnts : mnts;
  seconds = seconds < 10 ? "0" + seconds : seconds;
  val = hrs + "" + mnts + "" + seconds;
  return val;
};

String.prototype.toProperCase = function ()
{
  return this.replace(/\w\S*/g, function (txt)
  {
    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
  });
};

Number.prototype.pad = function (size)
{
  var s = String(this);
  while (s.length < (size || 2))
  {
    s = "0" + s;
  }
  return s;
};

String.prototype.replaceAll = function (str1, str2)
{
  return this.split(str1).join(str2);
};
