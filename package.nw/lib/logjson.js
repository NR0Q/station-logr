// Station Logr Copyright (C) 2021 Matthew Chambers, CBT
// All rights rreserved.
// See LICENSE for more information.
function ifLogExists(stationCall)
{
  var logFileName = sl_jsonDir + stationCall + ".json";
  try
  {
    if (fs.existsSync(logFileName))
    {
      return true;
    }
    else
    {
      return false;
    }
  }
  catch (e)
  {
    console.error(e);
  }
}

function createLogObject(logElements)
{
  var logData = {};
  var logObject = {};
  Object.keys(logElements).forEach(function (logDataPoint, i)
  {
    if (logElements[logDataPoint].name !== "logDateTime")
    {
      logData[logElements[logDataPoint].name] = logElements[logDataPoint].value;
    }
  });
  logObject = JSON.parse("{\"" + logElements.logDateTime.value + "\" :" + JSON.stringify(logData) + "}");
  return logObject
}

function saveNewFile(stationCall, logObject)
{
  var logFileName = sl_jsonDir + stationCall + ".json";
  var logString = "{ \"" + stationCall + "\":" + JSON.stringify(logObject) + "}"
  var newLog = JSON.parse(logString);
  writeLogFile(logFileName, newLog);
}

function writeLogFile(logFileName, fileContents)
{
  try
  {
    fs.writeFileSync(logFileName, JSON.stringify(fileContents, undefined, 4));
  }
  catch (e)
  {
    console.error(e);
  }
}

function readLogFile(stationCall)
{
  var logFileName = sl_jsonDir + stationCall + ".json";
  var logJSON = {};
  if (fs.existsSync(logFileName))
  {
    var fileBuf = fs.readFileSync(logFileName, "UTF-8");
    logJSON = JSON.parse(fileBuf);
  }
  return logJSON;
}

function appendLogFile(stationCall, logObject)
{
  var logFileName = sl_jsonDir + stationCall + ".json";
  var newLog = {};
  var existingLog = readLogFile(stationCall);
  if (Object.keys(existingLog) == stationCall)
  {
    var oldLogData = existingLog[stationCall];
    var newJSONString = "{ \"" + stationCall + "\":" + JSON.stringify(oldLogData).slice(0, -1) + "," + JSON.stringify(logObject).slice(1) + "}"
    newLog = JSON.parse(newJSONString);
    writeLogFile(logFileName, newLog);
  }
}
