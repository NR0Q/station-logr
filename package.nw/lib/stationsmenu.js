// Station Logr Copyright (C) 2021 Matthew Chambers, CBT
// All rights rreserved.
// See LICENSE for more information.
function renderStationArray()
{
  var worker = "<div id='stationArrayTitle'><h1>Log which station?</h1></div><div id='stationArray'>";
  Object.keys(sl_stationJson).forEach(function (key)
  {
    worker += "<div class='stationButton' id='" + key + "' onclick='stationLogForm(\"" + key + "\", \"logNew\")'>";
    if (sl_stationJson[key].logo !== "undefined" && fs.existsSync(sl_stationLogos + sl_stationJson[key].logo))
    {
      worker += "<img class='stationBtnImage' src='file:" + sl_stationLogos + sl_stationJson[key].logo + "'>";
    }
    else
    {
      worker += key;
    }
    worker += "</div>";
  });
  worker += "</div>";
  stationList.innerHTML = worker;
  stationForm.style.display = "none";
  stationList.style.display = "block";
  doExportLogs.style.display = "inline-block";
  returnToList.style.display = "none";
  saveStationLog.style.display = "none";
  exportStationLogs.style.display = "none";
}
