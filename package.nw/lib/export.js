// Station Logr Copyright (C) 2021 Matthew Chambers, CBT
// All rights rreserved.
// See LICENSE for more information.
function exportLogs()
{
  worker = "Export logs for the following stations:<div>";
  Object.keys(sl_stationJson).forEach(function (key)
  {
    worker += "<input class='exportStations' type='checkbox'id='export-" + key + "' name='" + key + "'/><label for='export-" + key + "'>" + key + "</label><br />";
  });
  worker += "</div>";
  exportStations.innerHTML = worker;
  doExportLogs.style.display = "none";
  returnToList.style.display = "inline-block";
  saveStationLog.style.display = "none";
  stationList.style.display = "none";
  stationForm.style.display = "none";
  exportStationLogs.style.display = "block";
}

function exportTimeCheck(changed)
{
  var startDate = exportStart.value;
  var endDate = exportEnd.value;
  var weekChecked = false;
  var sunSatChecked = false;
  if (limitWeek.checked)
  {
    limitSunSatDiv.style.display = "block";
    weekChecked = true;
  }
  else
  {
    limitSunSatDiv.style.display = "none";
    weekChecked = false;
    sunSatChecked = false;
  }

  if (limitSunSat.checked)
  {
    sunSatChecked = true;
  }
  else
  {
    sunSatChecked = false;
  }

  if (changed == "exportStart")
  {
    exportEnd.min = startDate;
    if (sunSatChecked)
    {
      startDate = nearestSunday(startDate);
      exportStart.value = startDate;
    }
    if (weekChecked)
    {
      exportEnd.value = addWeek(startDate);
    }
  }
  if (changed == "exportEnd")
  {
    exportStart.max = endDate;
    if (sunSatChecked)
    {
      endDate = nearestSaturday(endDate);
      exportEnd.value = endDate;
    }
    if (weekChecked)
    {
      exportStart.value = subtractWeek(endDate);
    }
  }
}

function subtractWeek(dateString)
{
  // dateString is yyyy-mm-dd
  let inputDate = new Date(dateString + "T00:00:00");
  inputDate.setDate(inputDate.getDate() - 6);
  let newDateString = inputDate.toISOString().slice(0, 10);
  return newDateString;
}

function addWeek(dateString)
{
  // dateString is yyyy-mm-dd
  let inputDate = new Date(dateString + "T00:00:00");
  inputDate.setDate(inputDate.getDate() + 6);
  let newDateString = inputDate.toISOString().slice(0, 10);
  return newDateString;
}

function nearestSunday(startDateStr)
{
  let startDate = new Date(startDateStr + "T00:00:00");
  while (startDate.getDay() > 0)
  {
    startDate.setDate(startDate.getDate() - 1);
  }
  let newDateStr = startDate.toISOString().slice(0, 10);
  return newDateStr;
}

function nearestSaturday(endDateStr)
{
  let endDate = new Date(endDateStr + "T00:00:00");
  while (endDate.getDay() < 6)
  {
    endDate.setDate(endDate.getDate() + 1);
  }
  let newDateStr = endDate.toISOString().slice(0, 10);
  return newDateStr;
}

function exportToFile()
{
  var stations = document.getElementsByClassName("exportStations");
  var startDate = exportStart.value;
  var endDate = exportEnd.value;
  var exportStations = {};
  if (startDate == "" || endDate == "")
  {
    alert("A valid date range must be selected to export!");
    return;
  }
  Object.keys(stations).forEach(function (call)
  {
    if (stations[call].checked)
    {
      exportStations[stations[call].name] = sl_stationJson[stations[call].name];
    }
  });
  if (Object.keys(exportStations).length == 0)
  {
    alert("A station must be selected to export!");
    return;
  }
  Object.keys(exportStations).forEach(function (call)
  {
    var texStrings = generateLogTable(exportStations, call, startDate, endDate);
    var finalExport = replaceLatexVars(sl_latexExport, texStrings);
    var output = fs.createWriteStream(sl_appData + call + "-" + startDate + "-" + endDate + ".pdf");
    console.log(finalExport);
    var options = { cmd: "pdflatex", passes: 4, inputs: "./texlib/" };
    var pdf = latex(finalExport, options);

    pdf.pipe(output);
    pdf.on("error", err => console.error(err));
    pdf.on("finish", () => alert("Log for " + call + " exported to PDF!"));
  });
}

function replaceLatexVars(template, newStrings)
{
  Object.keys(newStrings).forEach(function (myString)
  {
    let searchText = "\\" + myString + "{}";
    let replaceText = newStrings[myString];
    // console.log("s/" + searchText + "/" + replaceText + "/");
    template = template.replace(searchText, replaceText);
  });
  return template;
}

function generateLogTable(exportStations, call, startDateStr, endDateStr)
{
  let startDate = new Date(startDateStr + "T00:00:00");
  let endDate = new Date(endDateStr + "T23:59:59");
  let thisStation = exportStations[call];
  let latexStrings = {}
  latexStrings.graphicspath = "\\graphicspath{ {" + sl_stationLogos + "} }";
  latexStrings.slstationlogo = "\\slstationlogo{" + thisStation.logo + "}";
  latexStrings.slcallsign = "\\slcallsign{" + call + "}";
  latexStrings.slcityoflicense = "\\slcityoflicense{" + thisStation.metadata.city + "}";
  latexStrings.slsignpreamble = "\\slsignpreamble{" + call + " of " + thisStation.metadata.city;
  latexStrings.slsignpreamble += " during the week of " + startDateStr + " through " + endDateStr + ".}";

  latexStrings.slmetadata = "\\slmetadata{"
  Object.keys(thisStation.metadata).forEach(function (datarow)
  {
    latexStrings.slmetadata += metadataLabels(datarow) + thisStation.metadata[datarow] + "\\\\";
  });
  latexStrings.slmetadata += "}";

  latexStrings.sltableHeader = "\\sltableHeader{\\textbf{Date/Time} & ";
  latexStrings.sltabSubHeader = "\\sltabSubHeader{ & ";
  latexStrings.sltableDef = "\\sltableDef{\\begin{longtable}[h!]{l|";

  Object.keys(thisStation.log).forEach(function (logColumn)
  {
    thisLogTable = thisStation.log[logColumn];
    latexStrings.sltableHeader += "\\textbf{" + thisLogTable.label + "} & ";
    latexStrings.sltabSubHeader += thisLogTable.units + " & ";
    latexStrings.sltableDef += "c|";
  });
  latexStrings.sltableHeader += "\\textbf{Operations Notes}\\\\}";
  latexStrings.sltabSubHeader += " \\\\}";
  latexStrings.sltableDef += "l}}";

  latexStrings.sltableRows = "\\sltableRows{";
  let logData = readLogFile(call);
  let thisData = logData[call];
  Object.keys(thisData).forEach(function (timestamp)
  {
    let thisDate = new Date(timestamp);
    if (dates.inRange(thisDate, startDate, endDate))
    {
      let thisRow = thisData[timestamp];
      latexStrings.sltableRows += timestamp;
      Object.keys(thisRow).forEach(function (column)
      {
        latexStrings.sltableRows += " & " + thisRow[column];
      });
      latexStrings.sltableRows += "\\\\";
    }
  });
  latexStrings.sltableRows += "}";

  return latexStrings;
}
