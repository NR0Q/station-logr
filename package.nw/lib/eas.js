// Station Logr Copyright (C) 2021 Matthew Chambers, CBT
// All rights rreserved.
// See LICENSE for more information.

var sl_easStationLog = {};
var EASEvent = {
  rawHeader: "",
  originCode: "",
  originName: "",
  eventCode: "",
  eventName: "",
  locationCodes: [],
  locationNames: [],
  duration: "",
  start: "",
  callsgin: ""
}

function easEventParse(rawheader)
{
  let myEasEvent = new EASEvent();
  myEasEvent.originCode = getOriginCode(rawheader);
  myEasEvent.originName = getOriginName(myEasEvent.originCode);
  myEasEvent.eventCode = getEventCode(rawheader);
  myEasEvent.eventName = getEventName(myEasEvent.eventCode);
  myEasEvent.duration = getDuration(rawheader);
  myEasEvent.start = getEventStart(rawheader);
  myEasEvent.callsgin = getEASCallsign(rawheader);
  return myEasEvent;
}

function getOriginCode(inputStr)
{
  const regex = /(?:ZCZC)-([A-Z]{3})-(?:[A-Z]{3})(?:-[0-9]{6})+(?:\+(?:[0-9]{4}))-(?:[0-9]{7})-(?:[A-Z\\]{3,8})/mg;
  let outputStr = regex.exec(inputStr);
  return outputStr;
}

function getEventCode(inputStr)
{
  const regex = /(?:ZCZC)-(?:[A-Z]{3})-([A-Z]{3})(?:-[0-9]{6})+(?:\+(?:[0-9]{4}))-(?:[0-9]{7})-(?:[A-Z\\]{3,8})/mg;
  let outputStr = regex.exec(inputStr);
  return outputStr;
}

function getDuration(inputStr)
{
  const regex = /(?:ZCZC)-(?:[A-Z]{3})-(?:[A-Z]{3})(?:-[0-9]{6})+(?:\+([0-9]{4}))-(?:[0-9]{7})-(?:[A-Z\\]{3,8})/mg;
  let outputStr = regex.exec(inputStr);
  return outputStr;
}

function getEventStart(inputStr)
{
  const regex = /(?:ZCZC)-(?:[A-Z]{3})-(?:[A-Z]{3})(?:-[0-9]{6})+(?:\+(?:[0-9]{4}))-([0-9]{7})-(?:[A-Z\\]{3,8})/mg;
  let outputStr = regex.exec(inputStr);
  return outputStr;
}

function getEASCallsign(inputStr)
{
  const regex = /(?:ZCZC)-(?:[A-Z]{3})-(?:[A-Z]{3})(?:-[0-9]{6})+(?:\+(?:[0-9]{4}))-(?:[0-9]{7})-([A-Z\\]{3,8})/mg;
  let outputStr = regex.exec(inputStr);
  return outputStr;
}

/*
TO DO
1) Load EAS Events and FIPS on startup (StartupEngine)
2) Parser for Sage ENDEC Logs
3) Event to grab and parse logs when log is generated
4) Optional, Event to grab, parse and display logs on demand
5) Format EAS table for USL
*/
