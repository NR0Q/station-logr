# Format of stations.json file

Each station defined as an object in an array of objects for the market. The array can contain as few as one station and up to as many as you'd like to log from a single instance of Station Logr. I recommend limiting a single instance to a single market or a single Chief Operator for simplicity. The file format is standard JSON.

## Callsign

The key for each station "object" is the station callsign as it should appear logs.

Example for KTUX and KEEL, an FM station in Carthage, TX and AM station in Shreveport, LA...
```json
{
    "KTUX": {

    },
    "KEEL": {

    }
}
```
AM stations should contain only the root callsign without an appended -AM or (AM) and FM's should only contain the -FM if there are other stations that share the root callsign or that is how the station if officially licensed by the FCC. Translator callsigns should contain the full callsign such as K221AB without an additional (FX). Booster and multi-site syncronous systems can append a -1, -2 or FM1, FM2 as appropriate to match the FCC records. 

## Metadata

Metadata is the base information that describes the station and is what appears in Station Logr's log screen and PDF export headers. This should contain at a minimum the Facility ID, Service, Class, City of License, Frequency (and FM Channel if applicable), Power and Location. 

For our example station KTUX...
```json
"KTUX": {
    "metadata": {
        "facilityID": 35688,
        "service": "FM",
        "class": "C1",
        "city": "Carthage, TX",
        "channel": 255,
        "freq": "98.9 MHz",
        "power": "100kW ERP",
        "location": ["32-23-19.5 N", "94-01-10.6 W"]
    }
}
```
You can see that text data is stored inside double quotes "" and the location is a pair of text strings inside brackets []. Additional data fields can be stored such as height above average terrain (haat), broadcast schedule (time), and translator parent station (parent) or any other data you would like to have show up in log headers. 

## Logo

This field is not required but adds a bit of station branding to the program and log files. The logo files should ideally be transparent background PNG files saved in your Documents/StationLogr/logos directory. The logo file name is then specified as a text string with the "logo" key. 

For our example station KTUX...
```json
"KTUX": {
    "metadata": {
        "station metadata"
    },
    "logo": "ktux_highway989_logo.png"
}
```
## EAS

Here is where we specify how EAS logs are formatted and where those files will be located. At this time, I am only supporting the Sage Digital Endec (Model 3644) but other models will be supported as sample log files are provided. 

Here's how that would look for our example station...
```json
"KTUX": {
    "metadata": {
        "station metadata"
    },
    "eas": {
        "make": "sage",
        "model": "digital-endec",
        "file-path": "/file/path/to/EAS/logs/"
    }
}
```
Because I plan to eventually offer support for other manufacturers and models, there is a place to specifiy that here. The file-path should be the absolute system path or URI to where log files are stored for that station. Because it's theoriticly possible that each station could store it's log files to a different location and/or have different EAS equipment. I specify them here on a by-the-station basis. EAS log files are retrieved when logs are exported to PDF.

## Remote Control

Because Station Logr will be able to grab a set of readings automatically on a schedule, we need to know what make and model remote control, the method of retrieving logs, the IP address or file path and schedule of when to check for new readings, if not a file based method.

Here is an example of retreiving logs via SNMP from a Burk ArcPlus at 4 and 10AM and 4 and 10PM every day for our example station KTUX...
```json
"KTUX": {
    "metadata": {
        "station metadata"
    },
    "rc": {
        "make": "burk",
        "model": "arcplus",
        "method": "snmp",
        "ipaddr": "192.168.0.10",
        "faultdelay": 0,
        "schedule": [
            "04:00",
            "10:00",
            "14:00",
            "22:00"
        ]
    }
}
```
Currently only Burk ArcPlus and Broadcast Tools WVRC series are supported, additional makes and models will be added as details of those are made available to the developer. 

Method can be either snmp with an ipaddr specified or file with a absolute file path or URI specified. If using the file method, the local remote control or software (Such as Burk AutoPilot) saves a file on it's set schedule so we won't specify a schedule. Files will be retrieved when logs are exported to PDF and the data merged with locally captured data. 

For SNMP, Station Logr will monitor the specified IP address and when status channels change (such as for a fault, power outage, or pattern change) and capture a set of readings after the specified delay in seconds or 0 if no delay. This allows for a delay after an AM pattern or power change for the transmitter to settle. Station Logr will then take a set of readings at the specified schedule. You can specify as many readings as you want by listing the times for each reading in 24hr time. 

## Log Parameters

This is where we specify the readings that should be logged manually or captured automatically. 

```json
"KTUX": {
    "metadata": {
        "station metadata"
    },
    "log": {
        "power": {
            "label": "TPO",
            "units": "kW",
            "type": "number",
            "step": 0.1,
            "minimum": 0,
            "maximum": 35,
            "required": true,
            "rcchannel": 1
        },
        "paV": {
            "label": "PA V",
            "units": "volts",
            "type": "number",
            "step": 0.01,
            "minimum": 0,
            "maximum": 60,
            "required": true,
            "rchchannel": 2
        },
        "stackTemp": {
            "label": "Exhaust Temp",
            "units": "deg F",
            "type": "number",
            "step": 1,
            "minimum": 0,
            "required": true,
            "rcchannel": 5
        },
        "txFilter": {
            "label": "TX Air Filters",
            "units": "",
            "type": "dropdown",
            "options": [
                "Not Checked",
                "Good",
                "Fair",
                "Replaced",
                "Needs Replaced"
            ],
            "required": false
        },
        "towLights": {
            "label": "Tower Lights On",
            "type": "status",
            "rcchannel": 8
        }
    }
}

```

Types of log items are:
- Number - For power, voltage, current, temperature or other numeric values.
- Text - For textual data entry.
- Status - Typically stuff that is a true/false, such as faults, tower lights on, mains power ok. 
- Dropdown - This can be a status channel on the remote but has more of a A/B value such as Day/Night pattern or power, day or night tower strobes (for towers lit 24/7), main vs backup transmitter or for stuff that would be logged manually that requires the operator to inspect and decide the status of something, such as AC and transmitter air filters or generator fuel level. 

Each log item has a label, units, and required true/false. Number types have min/max and step values and dropdown has a set of options. If a reading is based on a status channel, a true, on or 1 value would equate to a status check box being checked or the second dropdown option selected. A false, off or 0 value would equate to a status check box being unchecked, or the first dropdown option. Units may be omitted by giving a log item a units value of a set of double ticks with nothing between them "". The remote control channel that each log item can also be specified by rcchannel.

