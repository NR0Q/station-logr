# station logr

Station Logr is a "Unified Station Log" management system that allows broadcast station chief operators/chief engineers and delegated station personnel to capture operations data and generate the weekly operations log for the station.

## Station Logr's features include
 - Manual data entry
 - Automatic capture from popular transmitter site remote control systems
 - Automatic capture from popular EAS Encoder/Decoder systems
 - Customizable with station branding
 - Easily define what data fields are to be captured and parameters for those fields.
 - Export a "Unified Station Log" to PDF format with a place for the chief operators digital signature
 - Software is open source allowing for stations to customize to their needs.
 - Runs on Windows, MacOS, Linux and Raspberry Pi systems (save money not buying PCs to sit at tower sites)

 Station Logr is Free Open Source Software (FOSS) under the terms of the BSD 3-Clause License and is available at no cost to all broadcasters except where local regulations may prohibt or have different requirements for station operations logging. (However, the developer may be able to work with stations in those jurisdictions to meet their needs.)

## Limitations
Automatic data capture may not be guaranteed for all transmitter/remote control combinations and in all network configurations. The computer running Station Logr needs access via LAN/VPN to the transmitters, remote controls to capture data and access to exported EAS files (typically a FTP server).

## Dependencies
Station Logr's export to PDF feature requires a compatible [LaTeX or TeX](https://www.tug.org/texlive/) and [pdftex](https://www.tug.org/applications/pdftex/) package.