Name:           station-logr
Summary:        Station Logr is an app for logging broadcast station operations.
Version:        0.1.1
Release:        1%{?dist}
BuildArch:      noarch
Source0:        %{name}-%{version}.tar.gz

License:        BSD 3-Clause License
URL:            https://gitlab.com/nr0q/station-logr
Group:          Science & Math
Packager:       Matthew Chambers <nr0q@gridtracker.org>
Requires:       nwjs
BuildRequires:  desktop-file-utils make nodejs

%description
Captures station operations data and generates all unified operations log
to archive as evidence of operations consistent with FCC rules and station
authorization documents.

%prep
%setup -q station-logr

%build

%install
DESTDIR=${RPM_BUILD_ROOT} make NO_DIST_INSTALL=true install

%check

%clean
DESTDIR=${RPM_BUILD_ROOT} make clean

%files
%{_datadir}/%{name}/
%{_datadir}/applications/%{name}.desktop
%{_datadir}/%{name}/
%{_bindir}/%{name}
%{_mandir}/man1/
%license %{_docdir}/%{name}/

%changelog
* Wed Nov 04 2021 Matthew Chambers <nr0q@gridtracker.org> - 0.1.1-1
- First attempt at repo grade RPM builds
